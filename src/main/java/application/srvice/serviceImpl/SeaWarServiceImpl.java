package application.srvice.serviceImpl;

import application.entity.Message;
import application.entity.Playerfield;
import application.entity.Serverfield;

import application.srvice.SeaWarService;
import org.springframework.stereotype.Service;

@Service
public class SeaWarServiceImpl implements SeaWarService {

  private Serverfield serverfield;
  public Playerfield playerfield;
  public Message message;




  @Override
  public Message createMessage(String shot) {
    if (serverfield==null) {
      serverfield=new Serverfield();
    }
    if (playerfield==null) {
      playerfield=new Playerfield();
    }
    for (;;) {
      message=new Message(String.valueOf(serverfield.check(shot)));
      if (playerfield.field[message.getServerShot().charAt(0)-'a'][Integer.parseInt(message.getServerShot().substring(1))]==0) {
        break;
      }
      else {
        continue;
      }
    }
    return message;
  }
}
