package application.entity;

import java.util.Random;

public class Message {
    private String serverAnswer;
    private String serverShot;
    public Message(String serverAnswer) {
        this.serverAnswer = serverAnswer;
        this.serverShot =(char)(new Random().nextInt(10)+(int)'a')+ String.valueOf(new Random().nextInt(10)+1);
    }

    public String getServerAnswer() {
        return serverAnswer;
    }

    public String getServerShot() {
        return serverShot;
    }

    public void setServerAnswer(String serverAnswer) {
        this.serverAnswer = serverAnswer;
    }

    public void setServerShot(String serverShot) {
        this.serverShot = serverShot;
    }

    @Override
    public String toString() {
        return "Message{" +
                "serverAnswer='" + serverAnswer + '\'' +
                ", serverShot='" + serverShot + '\'' +
                '}';
    }
}
