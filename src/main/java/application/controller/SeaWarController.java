package application.controller;


import application.entity.Message;
import application.srvice.serviceImpl.SeaWarServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeaWarController {

    private SeaWarServiceImpl seaWarService;


    @Autowired
    public SeaWarController(SeaWarServiceImpl seaWarService) {
        this.seaWarService = seaWarService;
    }

    @RequestMapping("/shoot")
    public Message shoot(@RequestParam(value="shot", required = false, defaultValue = "World") String shot) {
          return seaWarService.createMessage(shot);
    }
    @RequestMapping("/answer")
    public void answer(@RequestParam(value="answer", required = false, defaultValue = "World") int answer) {
        seaWarService.playerfield.fill(answer,seaWarService.message.getServerShot());


    }


}